import scala.reflect.runtime.{ currentMirror, universe }
import scala.tools.reflect.ToolBox

object Eval {
 val toolbox: ToolBox[universe.type] = currentMirror.mkToolBox()

 def apply[T](input: String): T =
   toolbox.eval(toolbox.parse(input)).asInstanceOf[T]
}

package com {
 package hoge {
   case class Exchange(
       machineId: Int,
       id: String = ""
   )
 }
}

object Main extends App {
 import com.hoge.Exchange

 val as =
   """
     |import com.hoge.Exchange
     |
     |(exchange: Exchange) => {
     |   exchange.machineId match {
     |     case x if x % 2 == 0 => 0.15
     |     case _ => 0.10
     |   }
     |}
   """.stripMargin

 val rateFunc = Eval.apply[Exchange => Double](as)

 val ex1 = Exchange(10)
 val ex2 = Exchange(9)
 println(rateFunc(ex1))
 println(rateFunc(ex2))
}
