# scala-reflection-sample

# 検証ライブラリ
- scala-reflect
- scala-compiler (intelliJで動かすには必要)

# run（replディレクトリ内のファイル：dockerで実行）
```
$ cd repl
$ docker build -t scala-reflection-runtime-repl ./
$ docker run -v $(pwd):/app scala-reflection-runtime-repl ${ファイル名}
```

ex)
```
$ cd repl
$ docker run -v $(pwd):/app scala-reflection-runtime-repl hello.scala
cat: /docker-java-home/release: No such file or directory
cat: /docker-java-home/release: No such file or directory
hello
```

# run (scalacディレクトリ内のファイル：dockerで実行）
- REPLではpackageを指定できないらしい (https://stackoverflow.com/a/25343321/6152413)
- eval対象文字列で独自クラスなど使いたい場合はpackage名つけてimportする必要ある
↑ために、scalacで実行する
```
$ cd scalac
$ docker build -t scala-reflection-runtime-scalac ./
$ docker run -v $(pwd):/app -e FILENAME=${ファイル名} scala-reflection-runtime-scalac
```

ex)
```
$ cd scalac
$ docker run -v $(pwd):/app -e FILENAME=hello.scala scala-reflection-runtime-scalac
```
