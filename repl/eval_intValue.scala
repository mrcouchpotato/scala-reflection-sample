// https://stackoverflow.com/a/31117964/6152413

import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox
val toolbox = currentMirror.mkToolBox()

val as = "2*(2+3)"
val compe = toolbox.eval(toolbox.parse(as))

println(compe.getClass) // prints class java.lang.Integer
println(compe) // prints 10
