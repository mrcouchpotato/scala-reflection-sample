// 正しくないscalaコードを使うと
// 実行時にtoolbox.evalでscala.tools.reflect.ToolBoxErrorが発生する
import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox

val toolbox = currentMirror.mkToolBox()

val as = "(i: Int) => hogehoge" // ただしくないscalaコード
println("---- 1 -----")
val x = toolbox.parse(as)
println("---- 2 -----")
val x2 = toolbox.eval(x)
println("---- 3 -----")
val compe: Int => Int = x2.asInstanceOf[Int => Int]

println(compe.getClass)
println(compe(2))
