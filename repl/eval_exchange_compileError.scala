// 独自型をeval対象の文字列に含むとevalでエラー

import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox

case class Exchange(
    machineId: Int,
    id: String = ""
)

val toolbox = currentMirror.mkToolBox()

val as =
  """
    |(exchange: Exchange) => {
    |   exchange.machineId match {
    |     case x if x % 2 == 0 => 0.15
    |     case _ => 0.10
    |   }
    |}
  """.stripMargin

println("-------- 1 ---------")
val x = toolbox.parse(as)
println("-------- 2 ---------")
val x2 = toolbox.eval(x)
println("-------- 3 ---------")
val rateFunc = x2.asInstanceOf[Exchange => Double]

val ex1 = Exchange(10)
val ex2 = Exchange(9)
println(rateFunc(ex1))
println(rateFunc(ex2))
