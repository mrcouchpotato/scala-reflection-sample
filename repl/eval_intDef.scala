// キャストすることで強制的に関数だと認識させる

import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox
val toolbox = currentMirror.mkToolBox()

val as = "(i: Int) => i * 2"
val compe = toolbox.eval(toolbox.parse(as)).asInstanceOf[Int => Int]

println(compe.getClass) 
println(compe(2))
