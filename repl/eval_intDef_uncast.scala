// キャストしないと compe はAnyなので、このファイル自体のコンパイルエラー発生

import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox
val toolbox = currentMirror.mkToolBox()

val as = "(i: Int) => i * 2"
val compe = toolbox.eval(toolbox.parse(as))

println(compe.getClass) 
println(compe(2))
