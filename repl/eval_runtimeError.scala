// ランタイムエラーが発生するscalaコードは、
// evalした時ではなく、evalした結果を使う時にエラーが発生する
import scala.reflect.runtime.currentMirror
import scala.tools.reflect.ToolBox

val toolbox = currentMirror.mkToolBox()

val as = "(i: Int) => i / 0" // ランタイムエラーが発生するscalaコード
println("---- 1 -----")
val x = toolbox.parse(as)
println("---- 2 -----")
val x2 = toolbox.eval(x)
println("---- 3 -----")
val compe: Int => Int = x2.asInstanceOf[Int => Int]

println(compe.getClass)
println(compe(2))
